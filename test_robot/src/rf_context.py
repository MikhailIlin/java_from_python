from robot.running.context import EXECUTION_CONTEXTS

from robot.result import TestSuite
from robot.variables import VariableScopes
from robot.running.context import EXECUTION_CONTEXTS
from robot.running.namespace import Namespace
from robot.conf import RobotSettings
from robot.output.output import Output
from robot.running.model import ResourceFile

def context_create():
    _options = {"critical": "regression",
                "output_directory": "C:\\Examples\\RF",
                "output": "output.xml",
                "log_level": "INFO"}
    result = TestSuite(name="test_case",
                       doc="")
    settings = RobotSettings(**_options)
    _variables = VariableScopes(settings)
    _output = Output(settings)
    _resource = ResourceFile()
    ns = Namespace(_variables, result, _resource)
    ns.start_suite()
    EXECUTION_CONTEXTS.start_suite(result, ns, _output)

