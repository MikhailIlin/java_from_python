# Использование библиотеки JavaSwingLibrary для тестирования java приложений

Примеры использования библиотеки JavaSwingLibrary для тестирования приложения на java из python

### Что нужно для запуск примеров

1. Установить robotframework pip install robotframework
2. Скачать библиотеку JavaSwingLibrary
3. Прописать путь до библиотеки в переменную среды PYTHONPATH

### Что содержится в репозитории

1. remoteswinglibrary-2.2.4.jar (сама библиотека RemoteSwingLibrary)
2. SimpleAdder.jar DVDTool.jar (тестовые приложения на java)
3. test_robot (примеры работы с JavaSwingLibrary из python)
    * test.py dvd_test.py (примеры работы с приложениями Adder и DVDTool)
    * rf_context.py пример создания контекста выполнения, который нужен для 
    работы с другими библиотеками RF. Test.py содержит примеры использования
    библиотек BuilIn и ScreenShot. Для работы с JavaSwingLibrary он не обязателен)
4. bdd_jsl_test (пример работы с JavaSwingLibrary и behave)
### Примеры содержат разные варианты поиска объектов : 

* по имени объекта
* по индексу
* по надписи на кнопке