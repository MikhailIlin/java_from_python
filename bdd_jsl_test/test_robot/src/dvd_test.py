from RemoteSwingLibrary import RemoteSwingLibrary
from collections import namedtuple

def main():
    java_app = RemoteSwingLibrary()

    java_app.start_application(alias="Adder",
                               command='java -jar "C:\Program Files\jubula_8.7.1.046\examples\AUTs\SimpleAdder\swing\SimpleAdder.jar"',
                               remote_port=50000)
    java_app.start_application(alias="DVDTool",
                               command='java -jar "C:\Program Files\jubula_8.7.1.046\examples\AUTs\DVDTool\DVDTool.jar"',
                               remote_port=50001)
    java_app.run_keyword("Select Window", ["regexp=^DVD.*"])
    # java_app.run_keyword("List Components in Context", ["formatted"])
    add_category(java_app, "1 Категория")
    java_app.run_keyword("Select Window", ["regexp=^DVD.*"])
    add_category(java_app, "2 Категория")
    java_app.run_keyword("Select Window", ["regexp=^DVD.*"])
    java_app.run_keyword("Click On Tree Node", ["categoryTree", "1 Категория"])
    java_app.run_keyword("Select Window", ["regexp=^DVD.*"])
    add_dvd(java_app,["Лучшее", "Joe Dassin", "Pop sound", 1972, True])
    add_dvd(java_app,["Альбом", "Joe Dassin", "Мелодия", 1975, False])
    # table_Headers = java_app

    row = java_app.run_keyword("Find Table Row",["table", "Joe Dassin","Actor"])
    # row_Values
    row = java_app.run_keyword("Find Table Row",["table", "Мелодия","Director"])
    # print(row)

def add_category(java_app, category):
    java_app.run_keyword("Select From Menu", ["Edit|Add category..."])
    java_app.run_keyword("Dialog Should Be Open", ["Input"])
    java_app.run_keyword("Select Dialog", ["Input"])
    java_app.run_keyword("Insert Into Text Field", ["OptionPane.textField",category])
    java_app.run_keyword("Push Button", ["OK"])

def add_dvd(java_app, values):
    java_app.run_keyword("Select From Menu", ["Edit|Add DVD..."])
    row_count = java_app.run_keyword("Get Table Row Count",["table"])
    for k in range(len(values)):
        java_app.run_keyword("Type Into Table Cell",["table",row_count-1,k,values[k]])

def row_tuple(name, headers):
    return namedtuple(name, headers)

if __name__ == '__main__':
    main()
