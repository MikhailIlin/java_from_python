from robot.running.context import EXECUTION_CONTEXTS
from RemoteSwingLibrary import RemoteSwingLibrary
from robot.libraries.Screenshot import Screenshot
import rf_context
from robotbackgroundlogger import BackgroundLogger
from robot.libraries.BuiltIn import BuiltIn, register_run_keyword


def main():
    
    rf_context.context_create()
    context = EXECUTION_CONTEXTS.current
    screenshot = Screenshot(screenshot_directory="C:\Examples\RF")
    java_app = RemoteSwingLibrary()
    java_app.start_application(alias="Adder",
                               command='java -jar "C:\Program Files\jubula_8.7.1.046\examples\AUTs\SimpleAdder\swing\SimpleAdder.jar"',
                               remote_port=50000)
    java_app.switch_to_application(alias="Adder")
    java_app.run_keyword("Select Window", ["Adder"])
    java_app.run_keyword("Insert Into Text Field", [0, "4"])
    java_app.run_keyword("Insert Into Text Field", [1, "3"])
    java_app.run_keyword("Push Button", ["="])
    res = java_app.run_keyword("Get Text Field Value", [2])
    BuiltIn().should_be_equal(res, "7")
    screenshot.take_screenshot()
    # java_app.run_keyword("Select From Menu", ["File|quit"])


if __name__ == "__main__":
    main()
