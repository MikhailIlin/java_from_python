from behave import *


@given('Старт приложения "{alias}" командой: [{command}]')
def step(context, alias, command):
    context.java_app.start_application(alias=alias, command=command)


@when('Выбрать окно "{window}"')
def step(context, window):
    context.java_app.run_keyword("Select Window", [window])


@then('Ввести значение "{value}" в поле "{field}"')
def step(context, field, value):
    context.java_app.run_keyword("Insert Into Text Field", [field, value])


@then('Нажать кнопку "{button}"')
def step(context, button):
    context.java_app.run_keyword("Push Button", [button])


@when('Получить значение из поля "{field}"')
def step(context, field):
    context.response = context.java_app.run_keyword("Get Text Field Value", [field])


@then('Должно быть равно "{value}"')
def step(context, value):
    assert context.response == value


@when('Выбрать пункт меню "{menuitem}"')
def step(context, menuitem):
    context.java_app.run_keyword("Select From Menu", [menuitem])
