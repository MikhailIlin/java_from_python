Feature: Тестирование приложения Adder
    Scenario: Старт приложения
        Given Старт приложения "Adder" командой: [java -jar "C:\Program Files\jubula_8.7.1.046\examples\AUTs\SimpleAdder\swing\SimpleAdder.jar"]
        When  Выбрать окно "Adder"
    Scenario Outline: Проверка операций
        Then Ввести значение <val1> в поле "value1"
        And  Ввести значение <val2> в поле "value2"
        And  Нажать кнопку "="
        When Получить значение из поля "sum"
        Then Должно быть равно <sum>
        Examples: Сложение
            |val1 |val2 |sum |
            |"5"  |"4"  |"9" |
            |"53" |"44" |"97"|
            |"15" |"14" |"29"|
    Scenario: Закрыть приложение
        When Выбрать пункт меню "File|quit"
