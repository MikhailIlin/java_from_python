from tkinter import *
from tkinter import messagebox as mb
from tkinter import simpledialog as sd
from tkinter import ttk
from RemoteSwingLibrary import RemoteSwingLibrary
from collections import namedtuple
import re

Component_info = namedtuple('Component_info', ["level", "component", "index", "name"])


class Inspector(Frame):
    def __init__(self, parent):
        Frame.__init__(self, parent)
        self.parent = parent
        self.parent.protocol("WM_DELETE_WINDOW", self.close)
        self.java_app = None
        self.port = None
        self.statusbar = None
        self.tree = None
        self.win_menu = None
        self.active_win = None
        self.win_frame = None
        self.win = None
        self.display_types = ("Категории", "Дерево")
        self.display_type = StringVar()
        self.display_type.set(self.display_types[0])
        self.display_type.trace("w", self.change_dispalay_type)
        self.current_contextname = None
        self.current_contexttype = None
        self.init_menu()
        self.init_ui()

    def init_ui(self):
        top_frame = Frame(self.parent, relief=GROOVE, bg="#a9a9a9")
        Button(top_frame, text="Подключиться", command=self.connect_click, width=15).pack(side=LEFT, padx=10, pady=10)
        Button(top_frame, text="Обновить", command=self.refresh_click, width=15).pack(side=LEFT, padx=10, pady=10)
        Button(top_frame, text="Выход", command=self.close, width=15).pack(side=RIGHT, padx=10, pady=10)
        top_frame.pack(side=TOP, fill=X)
        self.statusbar = Label(self.parent, text="", bd=1, relief=SUNKEN, anchor=W)
        self.statusbar.pack(side=BOTTOM, fill=X)
        self.update_statusbar()
        self.active_win = StringVar()
        self.active_win.trace("w", self.change_window)
        win_frame = Frame(self.parent, relief=RAISED)
        self.win_menu = OptionMenu(win_frame, self.active_win, "")
        self.win_menu.pack(side=LEFT, fill=X)
        Button(win_frame, text="+ Dialog", command=self.add_dialog, width=15).pack(side=LEFT, padx=10, pady=10)
        ttk.Combobox(win_frame, textvariable=self.display_type, values=self.display_types, width=27).pack(side=LEFT,
                                                                                                       padx=10, pady=10)
        win_frame.pack(side=TOP, fill=X)
        self.init_tree()

    def init_tree(self):
        self.tree = ttk.Treeview(self.parent, selectmode=BROWSE)
        self.tree.bind('<<TreeviewSelect>>', self.component_indicate)
        ysb = ttk.Scrollbar(self.parent, orient='vertical', command=self.tree.yview)
        xsb = ttk.Scrollbar(self.parent, orient='horizontal', command=self.tree.xview)
        self.tree.configure(yscroll=ysb.set, xscroll=xsb.set)
        self.tree["column"] = ("Index", "Name")
        self.tree.column("#0", width=500, minwidth=500, stretch=NO)
        self.tree.column("Index", width=100, minwidth=100, stretch=NO)
        self.tree.column("Name", width=200, minwidth=200, stretch=NO)
        self.tree.heading("#0", text="Component", anchor=W)
        self.tree.heading("Index", text="Index", anchor=W)
        self.tree.heading("Name", text="Name", anchor=W)
        ysb.pack(side=RIGHT, fill=Y)
        xsb.pack(side=BOTTOM, fill=X)
        self.tree.pack(side=TOP, expand=1, fill=BOTH)

    def component_indicate(self, *args):
        row = self.tree.selection()
        item = self.tree.item(row[0])
        name = item["values"][1]

        if (item["text"].split(".")[-1] in ["JTextField", "JTree", "JTable", "JList"]) and not (name in ["null", ""]):
            temp_name = name.split(".")[-1]
            self.java_app.run_keyword("Select Window", [self.active_win.get()])
            self.java_app.run_keyword("Focus To Component", [temp_name])

    def update_statusbar(self):
        connected = bool(self.port)
        self.statusbar["text"] = f"Connected: {connected} Port: {self.port}"

    def init_menu(self):
        main_menu = Menu(self.parent)
        self.parent.config(menu=main_menu)
        file_menu = Menu(main_menu, tearoff=0)
        file_menu.add_command(label="Подключиться", command=self.connect_click)
        file_menu.add_separator()
        file_menu.add_command(label="Выход", command=self.close)
        main_menu.add_cascade(label="Файл", menu=file_menu)

    def close(self):
        self.quit()

    def refresh_click(self):
        self.update_list_win()

    def connect_click(self):
        self.port = sd.askinteger("Порт", "Порт подключения")
        if self.port:
            self.init_connect()

    def init_connect(self):
        try:
            self.java_app = RemoteSwingLibrary()
            self.java_app.application_started(alias="Application", remote_port=self.port)
            self.update_list_win()

        except Exception as ex:
            mb.showerror("Error", ex.__str__())
            self.port = None

        finally:
            self.update_statusbar()

    def update_list_win(self):
        menu = self.win_menu["menu"]
        menu.delete(0, 'end')
        list_win = self.java_app.run_keyword("List Windows", [])
        self.java_app.run_keyword("Select Main Window", [])
        current = self.java_app.run_keyword("Get Current Context", [])
        for item in list_win:
            if item != "":
                menu.add_command(label=item, command=lambda item=item: self.active_win.set(item))
        self.active_win.set(current)

    def add_dialog(self):
        dialog_name = sd.askstring("Dialog", "Наименование окна диалога")
        if dialog_name:
            self.current_contextname = dialog_name
            self.current_contexttype = "Dialog"
            self.update_tree()

    def change_window(self, *args):
        self.java_app.run_keyword("Select Window", [self.active_win.get()])
        str_components = self.java_app.run_keyword("List Components In Context", ["formatted"])
        # self.update_tree(, self.get_components_info(str_components))
        self.current_contextname = self.active_win.get()
        self.current_contexttype = "Window"
        self.update_tree()

    def change_dispalay_type(self, *args):
        self.update_tree()

    def update_tree(self):
        if self.display_type.get() == self.display_types[0]:
            self.update_by_category()
        elif self.display_type.get() == self.display_types[1]:
            self.update_by_treeview()

    def update_by_treeview(self):
        def add_child(parent, parent_level):
            nonlocal list_comp
            nonlocal component
            while (int(component.level)) == parent_level + 1:
                node = self.tree.insert(parent, END, text=component.component, open=True,
                                        values=(component.index, component.name))
                try:
                    component = next(list_comp)
                except StopIteration:
                    return
                if (int(component.level)) == parent_level + 2:
                    add_child(node, parent_level + 1)

        if self.win:
            self.tree.delete(self.win)
        self.win = self.tree.insert("", 0, text=self.current_contexttype, open=True, values=("", self.current_contextname))
        list_comp = iter(self.get_components_info())
        component = next(list_comp)
        add_child(self.win, -1)

    def update_by_category(self):
        if self.win:
            self.tree.delete(self.win)
        self.win = self.tree.insert("", 0, text=self.current_contexttype, open=True, values=("", self.current_contextname))
        category = {}
        for component_info in self.get_components_info():
            component_type, index, name = component_info.component, component_info.index, component_info.name
            type = component_type.split(".")[-1]
            if category.get(type):
                parent = category[type]
            else:
                parent = self.tree.insert(self.win, END, text=type, values=("", ""))
                category[type] = parent
            self.tree.insert(parent, END, text=component_type, values=(index, name))

    def get_components_info(self):
        def get_component_info(comp_info):
            list = re.findall(r"Level: (.*) Component: (.*) Index: (.*) Name: (.*)", comp_info)
            return Component_info(*list[0])

        result = []
        self.java_app.run_keyword(f"Select {self.current_contexttype}", [self.current_contextname])
        str_components = self.java_app.run_keyword("List Components In Context", ["formatted"])
        list_comp = str_components[1:-1].strip().split(",")
        for item in list_comp:
            result.append(get_component_info(item.strip()))
        return result


def main():
    root = Tk()
    root.title("JSL inspector")
    root.geometry("800x600-10+10")
    root.iconbitmap("./resource/icons8_spy.ico")
    app = Inspector(root)
    root.mainloop()


if __name__ == '__main__':
    main()
